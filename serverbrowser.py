#!/bin/python3

import sys
import os
import platform
import tkinter as tk
import webbrowser
import time
import threading
import re
import vdf
import configparser
import subprocess
from threading import Thread, Lock, enumerate
from datetime import datetime
from tkinter import ttk, Menu, BooleanVar
from steam import game_servers as gs

# Windows specific stuff
if platform.system() == "Windows":
    import winreg
    from ctypes import windll
    print("Windows detected, enabling HiDPI support")
    windll.shcore.SetProcessDpiAwareness(1)

APPID = 218
GAMEDIR = "gesource"

def resource_path(relative_path):
    """ Get the absolute path to the resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def get_servers():

    # Base query string. Get servers with the specified APPID and GAMEDIR
    query_str = '\\appid\\' + str(APPID) + '\\gamedir\\' + str(GAMEDIR)

    # Get the server listing from Steam
    try:
        servers = gs.query_master(query_str, max_servers = 1000)
    except Exception as err:
        print("Error retrieving listing from Steam master servers. " + str(err))
        return err

    # Return the server list (pair of ip, port)
    return servers

# Note, however, this list includes ALL favorites for the AppID.
# Servers will need to be queried individually to narrow down the list
# by looking at the gamedir.
def get_favorites():
    if platform.system() != "Windows":
        print("Favorites are not supported on platforms other than Windows")
        return []

    # Get the location of the steam installation
    try:
        hkcu = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)
        key_path = 'SOFTWARE\\Valve\\Steam'
        with winreg.OpenKey(hkcu, key_path) as key:
            steamPath = winreg.QueryValueEx(key, 'SteamPath')[0]
    except FileNotFoundError:
        print(f"Error: Registry path {key_path} not found.")
        print("Favorites will be disabled.")
        return []
    except Exception as err:
        print(f"Error reading registry: {err}")
        print("Favorites will be disabled.")
        return []

    ips = []
    dataDir = steamPath + '/userdata/'
    
    if os.path.isdir(dataDir):
        for dir in os.listdir(dataDir):
            vdfPath = dataDir + dir + "/7/remote/serverbrowser_hist.vdf"

            print('Loading favorites from ' + vdfPath)
            try:
                vdfData = vdf.load(open(vdfPath))

                # Because the case of the key names can't be guaranteed
                for key in vdfData:
                    if str(key).lower() == "filters":
                        filterKey = key

                for key in vdfData[filterKey]:
                    if str(key).lower() == "favorites":
                        favoritesKey = key

                favorites = vdfData[filterKey][favoritesKey]

            except Exception as err:
                print('Error loading file: ' + str(err))
                continue

            # Add server to list
            for server in favorites:
                if 'address' in favorites[server]:
                    server_addr = favorites[server]['address'].split(':')

                    if len(server_addr) == 1:
                        server_addr.append(27015)

                    # Convert to pair, not list
                    server_addr = (server_addr[0], int(server_addr[1]))

                    ips.append(server_addr)


    print("Favorite servers discovered: " + str(ips))
    return ips

def init_prefs():
    global preferences
    global config_root
    global config_path

    preferences = {
        "closeonconnect": BooleanVar(value=True),
        "sortafterrefresh": BooleanVar(value=True),
        "secure": BooleanVar(value=False),
        "nopassword": BooleanVar(value=False),
        "notempty": BooleanVar(value=False),
        "notfull": BooleanVar(value=False)
    }

    if platform.system() == "Windows":
        config_root = os.getenv('APPDATA') + '\\ges-serverbrowser'
        config_path = config_root + '\\config.ini'
    else:
        config_root = os.getenv('HOME') + '/.config/ges-serverbrowser'
        config_path = config_root + '/config.ini'

    print('Loading config file: ' + config_path)

    if not os.path.isfile(config_path):
        print('Config file doesn\'t exist. Using default config')
        return

    try:
        config = configparser.ConfigParser()
        config.read(config_path)
    except Exception as err:
        print('Error reading config file: ' + str(err))
        return

    if not 'config' in config.sections():
        print("Empty or invalid config file (missing key 'config'): " + config_path)
        return

    if 'closeonconnect' in config['config']:
        if config['config']['closeonconnect'] == "1":
            preferences['closeonconnect'].set(True)
        else:
            preferences['closeonconnect'].set(False)

    if 'sortafterrefresh' in config['config']:
        if config['config']['sortafterrefresh'] == "1":
            preferences['sortafterrefresh'].set(True)
        else:
            preferences['sortafterrefresh'].set(False)

    if 'secure' in config['config']:
        if config['config']['secure'] == "1":
            preferences['secure'].set(True)
        else:
            preferences['secure'].set(False)

    if 'nopassword' in config['config']:
        if config['config']['nopassword'] == "1":
            preferences['nopassword'].set(True)
        else:
            preferences['nopassword'].set(False)

    if 'notempty' in config['config']:
        if config['config']['notempty'] == "1":
            preferences['notempty'].set(True)
        else:
            preferences['notempty'].set(False)

    if 'notfull' in config['config']:
        if config['config']['notfull'] == "1":
            preferences['notfull'].set(True)
        else:
            preferences['notfull'].set(False)

def save_prefs():
    global preferences
    global config_root
    global config_path

    print('Saving config file: ' + config_path)

    config = configparser.ConfigParser()

    config['config'] = {}
    config['config']['closeonconnect'] = str( int( preferences['closeonconnect'].get() ) )
    config['config']['sortafterrefresh'] = str( int( preferences['sortafterrefresh'].get() ) )
    config['config']['secure'] = str( int( preferences['secure'].get() ) )
    config['config']['nopassword'] = str( int( preferences['nopassword'].get() ) )
    config['config']['notempty'] = str( int( preferences['notempty'].get() ) )
    config['config']['notfull'] = str( int( preferences['notfull'].get() ) )

    try:
        if not os.path.isdir(config_root):
            os.makedirs(config_root)

        with open(config_path, 'w') as configfile:
            config.write(configfile)

    except Exception as err:
        print('Error writing config file: ' + str(err))
        return

def now():
        return int(datetime.now().timestamp())

connectTime = 0

def connectToServer(server:str):
    global root
    global preferences
    global connectTime

    if len(server):
        # don't do anything if button pressed in the past second...
        # protects against double-clickers, and allows time for the game to load

        if connectTime > now() - 1:
            print("Ignoring connect button: it was clicked less than a second ago")
            return

        connectTime = now()

        print('Connecting to ' + server)
        webbrowser.open_new('steam://connect/' + server)

        if preferences['closeonconnect'].get():
            print("Closing program after connection to server as specified by user")
            close_program()

def timeIntToWord(time: int):
    if time <= 0:
        return ''

    time_str = ""

    hours = int(time / 3600)
    time -= hours * 3600
        
    minutes = int(time / 60)
    time -= minutes * 60

    seconds = time

    if hours:
        time_str += str(hours) + "h "
        
    if minutes:
        time_str += str(minutes) + "m "
        
    time_str += str(seconds) + "s"

    return time_str

# If tabbing over the treeview with the keyboard,
# select the first row
def focus_treeview(event):
    treeview = event.widget

    # Do nothing if something is already selected
    if treeview.selection():
        return

    children = treeview.get_children()

    if children:
        first_item = children[0]
        treeview.selection_set(first_item)
        treeview.focus(first_item)

def no_column_resize(event):
        if event.widget.identify_region(event.x, event.y) == "separator":
            return "break"

def entryRightClick(event):
    def copySelection(widget):
        try:
            selected_text = widget.selection_get()
            window.clipboard_clear()
            window.clipboard_append(selected_text)
            window.update()
        except tk.TclError:
            pass
        
    def delayed_popup(event):
        if event.keysym == "Menu":
            mnuRightClickEntry.tk_popup(event.widget.winfo_rootx(), event.widget.winfo_rooty(), 0)
        else:
            mnuRightClickEntry.tk_popup(event.x_root, event.y_root, 0)

    window = event.widget.winfo_toplevel()

    mnuRightClickEntry = Menu(window, tearoff=0)
    mnuRightClickEntry.add_command(label="Copy", state="disabled", command=lambda: copySelection(event.widget))

    if isinstance(event.widget, tk.Entry):
        try:
            if event.widget.selection_get():
                mnuRightClickEntry.entryconfig('Copy', state="active")
        except tk.TclError:
            pass

    window.after(100, lambda: delayed_popup(event))

def get_dpi():
    global SCALE
    screen = tk.Tk()
    current_dpi = screen.winfo_fpixels('1i')
    screen.destroy()
    SCALE = current_dpi / 96
    print(f'Setting DPI scale of {SCALE}')

def scaled(px: int):
    return round(px * SCALE)

tooltip_scheduled = False
tooltip_shown = False

def showTooltip(event):
    global tooltip_after_id
    global tooltip_scheduled
    global tooltip_shown
    global root

    def tooltip():
        global tooltip_window
        global tooltip_scheduled
        global tooltip_shown
        global root
        nonlocal event

        widget = event.widget
        widget_type = widget.winfo_class()

        print(f"Hovering over {widget_type}")
        
        if widget_type == 'TLabel':
            text = widget.cget("text")
            print(f"Mouse entered Label with text: {text}")
        
        elif widget_type == 'Treeview':
            row_id = widget.identify_row(event.y)
            column_id = widget.identify_column(event.x)
            if row_id and column_id:
                text = widget.item(row_id, "values")[int(column_id[1:]) - 1]  # Adjust index since columns start from #1

        elif widget_type == 'TButton':
            if widget.cget('text') == "Filters":
                text = "Options for narrowing down the server list"

            elif widget.cget('text') == "Refresh All":
                text = "Does a full refresh of the server list"

            elif widget.cget('text') == "Quick Refresh":
                text = "Refreshes the current list of servers"

            elif widget.cget('text') == "Connect":
                text = "Connect to the selected server"

        else:
            return

        if not 'text' in locals():
            return

        tooltip_window = tk.Toplevel(root)
        tooltip_label = tk.Label(tooltip_window, 
            text=f' {text} ', borderwidth=1, relief="solid")
        tooltip_label.pack()

        # remove window decorations
        tooltip_window.overrideredirect(True)

        # Calculate the coordinates for the tooltip window
        x = root.winfo_pointerx() + scaled(8)
        y = root.winfo_pointery() + scaled(8)
        tooltip_window.geometry("+{}+{}".format(x, y))

        tooltip_scheduled = False
        tooltip_shown = True

    # show tooltip after delay
    tooltip_scheduled = True
    tooltip_after_id = root.after(500, tooltip)

def hideTooltip(event):
    global tooltip_window
    global tooltip_after_id
    global tooltip_scheduled
    global tooltip_shown
    global root

    if tooltip_scheduled:
        root.after_cancel(tooltip_after_id)
        tooltip_scheduled = False

    if tooltip_shown:
        tooltip_window.destroy()
        tooltip_shown = False

def windowServerInfo(root, treeview):

    server_players = []
    lastSortedColumn = -1
    lastSortedReverse = False

    # Populates player list and sorts by specified column.
    def populateTreePlayers(column: int | bool = False):
        nonlocal treePlayers
        nonlocal server_players
        nonlocal lastSortedColumn
        nonlocal lastSortedReverse

        reverse = False

        # if sorting by score, default to reverse sort order
        if column == 1:
            reverse = True

        # If the argument is False, use last sorting options
        if isinstance(column, bool) and not column:
            column = lastSortedColumn
            reverse = lastSortedReverse
        else:
            # If we already clicked on this same column, sort in reverse order
            if lastSortedColumn == column:
                reverse = not lastSortedReverse

        # If the argument is an int, sort server_players by the specified column
        if isinstance(column, int):
            if column == 0:
                # cast to string, search case insensitively
                server_players = sorted(server_players, key = lambda player: str(player[column]).lower(), reverse = reverse)
            else:
                server_players = sorted(server_players, key = lambda player: player[column], reverse = reverse)

        treePlayers.delete(*treePlayers.get_children())

        for player in server_players:
            treePlayers.insert('', 'end', iid = player[0], 
                values = ( player[0], player[1], timeIntToWord(player[2]) ))

        # Scroll back to top
        treePlayers.yview_moveto(0)

        if isinstance(column, int):
            lastSortedColumn = column
            lastSortedReverse = reverse

    def refreshServerInfo():
        nonlocal server_addr
        nonlocal server_info
        nonlocal server_players
        nonlocal winServerInfo
        nonlocal lblName
        nonlocal lblGame
        nonlocal lblMap
        nonlocal lblPlayers
        nonlocal lblSecure
        nonlocal lblPing
        nonlocal treePlayers
        nonlocal lastRefresh
        server = server_addr.split(":")
        server = ( server[0], int(server[1]) )

        btnRefresh.config(state="disabled")
        lastRefresh = now()

        try:
            server_info = gs.a2s_info(server)
        except Exception as err:
            print("Error querying server info for " + str(server) + ": " + str(err))
            lblStatus.config(text = "Server is not responding")
            btnRefresh.config(state="normal")
            return

        try:
            server_players_a2s = gs.a2s_players(server)
        except Exception as err:
            server_players_a2s = False
            print("Error querying player info for " + str(server) + ": " + str(err))

        # Don't do anything if the user closed the window during refresh
        if not winServerInfo.winfo_exists():
            return

        lblName.config(text = server_info["name"])
        lblMap.config(text = server_info["map"])
        lblGame.config(text = server_info["game"])
        lblPlayers.config(text = str(server_info["players"]) + " / " + str(server_info["max_players"]))
        lblPing.config(text = str(int(server_info["_ping"])))
        lblStatus.config(text = "")

        if "vac" in server_info:
            if server_info["vac"]:
                lblSecure.config(text = "Secure")
            else:
                lblSecure.config(text = "Not Secure")

        if "visibility" in server_info:
            if server_info["visibility"]:
                lblStatus.config(text = "🔒 Password protected")
            else:
                lblStatus.config(text = "")

        if isinstance(server_players_a2s, bool) and not server_players_a2s:
            lblStatus.config(text = "Player list unavailable.")
            btnRefresh.config(state="active")
            return

        # get current selected player
        if treePlayers.selection():
            selectedPlayer = treePlayers.selection()[0]

        server_players = []

        # for filtering out duplicate names
        player_names = []

        for player in server_players_a2s:
            if not player["name"]:
                print('Skipping empty player name ' + str(player["name"]))
                continue

            if player["name"] in player_names:
                print('Skipping duplicate player name ' + str(player["name"]))
                continue

            name = re.sub(r'\^[0-9a-zA-Z]', '', player["name"])
            score = player["score"]
            time = int(player["duration"])

            server_players.append( [name, score, time] )
            player_names.append( player['name'] )

        populateTreePlayers()

        # Restore selected player
        if 'selectedPlayer' in locals():
            if selectedPlayer in treePlayers.get_children(""):
                treePlayers.selection_set(selectedPlayer)
                treePlayers.see(selectedPlayer)

        btnRefresh.config(state="normal")

    def refreshServerCallback():
        Thread(target=refreshServerInfo).start()

    def connectButtonClicked():
        global preferences
        
        connectToServer(server_addr)

        # don't attempt to close the window if it probably doesn't exist already
        if not preferences['closeonconnect'].get():
            winServerInfo.destroy()

    winServerInfo = tk.Toplevel(root)
    winServerInfo.geometry( str(scaled(400)) + "x" + str(scaled(450)) )
    winServerInfo.minsize( scaled(400), scaled(300) )
    winServerInfo.focus_set()

    if platform.system() == "Windows":
        winServerInfo.iconbitmap( resource_path("icon.ico") )

    # Timestamp of when the server info was last updated
    lastRefresh = 0

    # Minimum server info populated from the server list
    # The rest will be populated later by a query to the server
    server_addr = treeview.selection()[0]
    server_info = {
        'name': treeview.set(server_addr)['name'],
        'visibility': 0
    }

    padding = {'padx': 4, 'pady': 4}
    row0padding = {'padx': 4, 'pady': [8, 4]}

    print(f'Viewing server info for {server_addr}')

    winServerInfo.title("Game Info - " + server_info["name"])

    ttk.Label(winServerInfo, anchor="e", width=18, text="Name:").grid(column=0, row=0, **row0padding)
    ttk.Label(winServerInfo, anchor="e", width=18, text="IP Address:").grid(column=0, row=1)
    ttk.Label(winServerInfo, anchor="e", width=18, text="Game:").grid(column=0, row=2)
    ttk.Label(winServerInfo, anchor="e", width=18, text="Map:").grid(column=0, row=3)
    ttk.Label(winServerInfo, anchor="e", width=18, text="Players:").grid(column=0, row=4)
    ttk.Label(winServerInfo, anchor="e", width=18, text="Valve Anti-Cheat:").grid(column=0, row=5)
    ttk.Label(winServerInfo, anchor="e", width=18, text="Ping:").grid(column=0, row=6)

    lblName = ttk.Label(winServerInfo, anchor="w", text=server_info["name"])
    lblName.grid(column=1, row=0, sticky="w", **row0padding)
    
    entry = ttk.Entry(winServerInfo)
    entry.insert("end", server_addr)
    entry.config(state="readonly")
    entry.grid(column=1, row=1, sticky="nsew", **padding)
    entry.bind("<Button-3>", entryRightClick)
    entry.bind("<Menu>", entryRightClick)
    
    lblGame = ttk.Label(winServerInfo, anchor="w")
    lblGame.grid(column=1, row=2, sticky="w", **padding)
    lblMap = ttk.Label(winServerInfo, anchor="w")
    lblMap.grid(column=1, row=3, sticky="w", **padding)
    lblPlayers = ttk.Label(winServerInfo, anchor="w")
    lblPlayers.grid(column=1, row=4, sticky="w", **padding)
    lblSecure = ttk.Label(winServerInfo, anchor="w")
    lblSecure.grid(column=1, row=5, sticky="w", **padding)
    lblPing = ttk.Label(winServerInfo, anchor="w")
    lblPing.grid(column=1, row=6, sticky="w", **padding)

    lblName.bind("<Enter>", showTooltip)
    lblName.bind("<Leave>", hideTooltip)
    lblGame.bind("<Enter>", showTooltip)
    lblGame.bind("<Leave>", hideTooltip)
    lblMap.bind("<Enter>", showTooltip)
    lblMap.bind("<Leave>", hideTooltip)

    # Frame for the player list
    players_frame = ttk.Frame(winServerInfo)
    players_frame.grid(column=0, columnspan=2, row=7, sticky="nsew", **padding)

    # Treeview for the player list
    treePlayers = ttk.Treeview(players_frame, columns=("playername", "score", "time"), selectmode="browse")
    treePlayers.bind('<Button-1>', no_column_resize)
    treePlayers.bind('<FocusIn>', focus_treeview)

    treePlayers.column("#0", minwidth=0, width=0, stretch=False)
    treePlayers.heading("playername", text=" Player Name", anchor="w", command = lambda: populateTreePlayers(0))
    treePlayers.column("playername")
    treePlayers.heading("score", text=" Score", anchor="w", command = lambda: populateTreePlayers(1))
    treePlayers.column("score", minwidth=55, width=55, stretch=False)
    treePlayers.heading("time", text=" Time", anchor="w", command = lambda: populateTreePlayers(2))
    treePlayers.column("time", minwidth=100, width=100, stretch=False)

    treePlayers.grid(column=0, row=0, sticky="nsew")

    # Scrollbar
    players_scroll = ttk.Scrollbar(players_frame,
       orient = "vertical", 
       command = treePlayers.yview)
    players_scroll.grid(column=1, row=0, sticky="ns")

    treePlayers.configure(yscrollcommand=players_scroll.set)

    players_frame.grid_rowconfigure(0, weight=1)
    players_frame.grid_columnconfigure(0, weight=1)

    # Frame for the buttons
    button_frame = ttk.Frame(winServerInfo)
    button_frame.grid(column=0, columnspan=2, row=8, sticky="ew")

    # Status label, only used for errors
    lblStatus = ttk.Label(button_frame)
    lblStatus.grid(column=0, row=0, sticky="w", **padding)

    # Adding buttons to the frame
    btnJoin = ttk.Button(button_frame, text="Join Game", command = connectButtonClicked)
    btnJoin.grid(column=1, row=0, sticky="e", **padding)
    btnRefresh = ttk.Button(button_frame, text="Refresh", command=refreshServerCallback)
    btnRefresh.grid(column=2, row=0, sticky="e", **padding)
    btnClose = ttk.Button(button_frame, text="Close", command=winServerInfo.destroy)
    btnClose.grid(column=3, row=0, sticky="e", **padding)

    button_frame.grid_columnconfigure(0, weight=1)

    winServerInfo.grid_columnconfigure(1, weight=1)
    winServerInfo.grid_rowconfigure(7, weight=1)

    # Do initial refresh -- then, refresh every 10s thereafter
    def periodicRefresh():
        # Auto refresh, only if not done recently
        if lastRefresh < now() - 10:
            refreshServerCallback()
        winServerInfo.after(30000, periodicRefresh)

    periodicRefresh()

def close_program():
    global main_thread_exiting
    global root

    main_thread_exiting = True

    save_prefs()

    # hide the window while we wait for threads to exit
    # otherwise, the window might appear hung for a few seconds
    root.withdraw()
    
    thread_wait = 0

    # Wait for any threads to finish.
    # Kill the program if too much time elapses
    while len(threading.enumerate()) > 1:
        
        print('---')

        for thread in threading.enumerate():
            print('Waiting on ', str(thread))

        if thread_wait >= 5:

            print('Waiting too long for threads to exit. Killing self.')

            if platform.system() == "Windows":
                # shell=True prevents a console window from appearing
                subprocess.call( 'taskkill /f /pid ' + str(os.getpid()), shell=True )
            else:
                os.system( 'kill -9 ' + str(os.getpid()) )

        thread_wait += 1
        time.sleep(1)

    root.destroy()

def main():
    global preferences
    global root
    global main_thread_exiting

    get_dpi()

    main_thread_exiting = False

    serverList = {}
    favoritesList = {}
    disqualifiedServers = []
    threadLockServerList = Lock()
    threadLockFiltering = Lock()
    server_filter_pending = False
    refresh_in_progress = False
    stop_refresh = False

    # persistent values for sortServerList
    allLastSortedColumn = None
    allLastSortedReverse = False
    favsLastSortedColumn = None
    favsLastSortedReverse = False

    def showFilterMenu(btn, popup):
        try:
            x = btn.winfo_rootx()
            y = btn.winfo_rooty()
            popup.tk_popup(x, y, 0)
        finally:
            popup.grab_release()

    def showOptsMenu(event, menu):
        # Delay the popup menu
        # Prevents a release of the right mouse button from closing the menu
        def delayed_popup(event):
            if event.keysym == "Menu":
                x = event.widget.winfo_rootx()
                y = event.widget.winfo_rooty()
            else:
                x = root.winfo_pointerx()
                y = root.winfo_pointery()

            menu.tk_popup(x, y, 0)

        root.after(100, lambda: delayed_popup(event))

    def treeviewDoubleClick(event):
        treeview = event.widget
        iid = treeview.identify_row(event.y)
        region = treeview.identify_region(event.x, event.y)

        if not region == "cell" or not iid:
            return

        treeview.selection_set(iid)
        connectToServer(iid)

    def showServerOptsMenu(event, menu):
        treeview = event.widget

        # select row under mouse, but only if right-click was used
        if not event.keysym == "Menu":
            iid = treeview.identify_row(event.y)
            if iid:
                # mouse pointer over item
                treeview.selection_set(iid)
            else:
                # mouse pointer not over item
                # occurs when items do not fill frame
                # no action required
                return

        # Nothing selected
        if not treeview.selection():
            return

        iid = treeview.selection()[0]

        # Delay the popup menu
        # Prevents a release of the right mouse button from closing the menu
        def delayed_popup(event):
            if event.keysym == "Menu":
                x = event.widget.winfo_rootx()
                y = event.widget.winfo_rooty()
            else:
                x = root.winfo_pointerx()
                y = root.winfo_pointery()

            menu.tk_popup(x, y, 0)

        root.after(100, lambda: delayed_popup(event))

    # Updates the treeview "name" column to include the count of servers
    def updateServerCount(tab, treeview):
        nonlocal serverList

        shown = len(treeview.get_children())
        total = len(serverList)
        hidden = total - shown

        if tab == "all" and shown == 0 and hidden > 1:
            treeview.heading("name", text=' Servers (' + str(hidden) + ' excluded by filter)')
        else:
            treeview.heading("name", text=' Servers (' + str(shown) + ')')

    def serverToColumn(server: dict):
        """ Takes output from a2s_info and turns it into a tuple for insertion into treeview """
        name = ''
        if "name" in server:
            name = server['name']

        map = ''
        if "map" in server:
            map = server['map']

        pwd_required = ''
        if "visibility" in server:
            if server['visibility'] == 1:
                pwd_required = '🔒'

        secured = ''
        if "vac" in server:
            if server['vac'] == 1:
                secured = '✓'

        ping = 0
        if "_ping" in server:
            ping = str(int(server["_ping"]))

        game = ''
        if "game" in server:
            game = server['game']

        players = "0"
        if "players" in server:
            players = str(server['players'])

        if "max_players" in server:
            players += " / " + str(server['max_players'])

        return ( pwd_required, secured, name, game, map, players, ping )

    # Returns whether or not the server is in the scope of the current filter settings
    def serverInFilterScope(tab, server: dict):

        if tab == "favs":
            return True

        # set default values in case they weren't defined by this server somehow
        if not 'secure' in server:
            server['secure'] = 0

        if not 'visibility' in server:
            server['visibility'] = 0

        if not 'players' in server:
            server['players'] = 0

        if not 'max_players' in server:
            server['max_players'] = 0

        # check server info against filters

        if preferences['secure'].get():
            if not server['vac']:
                return False

        if preferences['nopassword'].get():
            if server['visibility']:
                return False

        if preferences['notempty'].get():
            if not server['players']:
                return False

        if preferences['notfull'].get():
            if server['players'] >= server['max_players']:
                return False

        return True

    # Filters the server list on demand (i.e. when changing filter options)
    def filterServerList(tab, treeview, acquireLock = True):
        global main_thread_exiting
        nonlocal server_filter_pending

        if tab == "all":
            localList = serverList

        if tab == "favs":
            localList = favoritesList

        threadLockFiltering.acquire()

        if acquireLock:
            threadLockServerList.acquire()

        treeview.delete(*treeview.get_children())

        for server in localList:
            if main_thread_exiting:
                threadLockFiltering.release()

                if acquireLock:
                    threadLockServerList.release()
                    
                return

            if serverInFilterScope(tab, localList[server]):
                treeview.insert( '', 'end', iid=server, values=serverToColumn(localList[server]) )

            # Update count in real time, but only if there's a lot of servers
            # A compromise that eliminates the quick flash effect in the server count
            # if there's not many servers
            if len(localList) > 100:
                updateServerCount(tab, treeview)

            # If there is a server sort/filter pending, exit early so it doesn't wait on us
            if server_filter_pending:
                print('Server sort pending, quitting')
                server_filter_pending = False

                threadLockFiltering.release()

                if acquireLock:
                    threadLockServerList.release()

                return

        updateServerCount(tab, treeview)

        threadLockFiltering.release()

        if acquireLock:
            threadLockServerList.release()


    # Sorts the server list treeview according to the selected column
    def sortServerList(tab, treeview, column: str | bool = False):
        global preferences
        nonlocal serverList
        nonlocal favoritesList
        nonlocal allLastSortedColumn
        nonlocal allLastSortedReverse
        nonlocal favsLastSortedColumn
        nonlocal favsLastSortedReverse

        threadLockServerList.acquire()

        # ReSort will be used to determine whether to attempt to keep the same selection
        reSort = False
        reverse = False

        if tab == "all":
            localList = serverList
            lastSortedColumn = allLastSortedColumn
            lastSortedReverse = allLastSortedReverse

        if tab == "favs":
            localList = favoritesList
            lastSortedColumn = favsLastSortedColumn
            lastSortedReverse = favsLastSortedReverse

        # If sorting by these columns, reverse the default sort order
        if column == "players" or column == "vac" or column == "visibility":
            reverse = True

        # If the column is False, use last sorting options
        if not column:
            column = lastSortedColumn
            reverse = lastSortedReverse
            reSort = True
        else:
            # If the column is the same as last time, reverse sort order
            # If we already clicked on this same column, sort in reverse order
            if lastSortedColumn == column:
                reverse = not lastSortedReverse

        # If column is None, do nothing
        # This is here to avoid an error if we're doing a re-sort on refresh
        # and the user hasn't clicked on a column yet
        if not column:
            threadLockServerList.release()
            return

        print('Sorting by (tab, column, reverse): ' + str(tab) + ', ' + str(column) + ', ' + str(reverse))

        # If the argument is a string, sort servers by the specified column
        if isinstance(column, str):
            if column == 'players' or column == '_ping':
                localList = dict(sorted(localList.items(), key = lambda item: item[1][column], reverse = reverse))
            else:
                # cast to string, case insensitive matching
                localList = dict(sorted(localList.items(), key = lambda item: str(item[1][column]).lower(), reverse = reverse))

        if column:
            lastSortedColumn = column
            lastSortedReverse = reverse

        if tab == "all":
            serverList = localList
            allLastSortedColumn = lastSortedColumn
            allLastSortedReverse = lastSortedReverse

        if tab == "favs":
            favoritesList = localList
            favsLastSortedColumn = lastSortedColumn
            favsLastSortedReverse = lastSortedReverse

        # Get the current selected item
        if treeview.focus():
            selected = treeview.focus()
        else:
            selected = False

        # Repopulate treeview, according to the filter settings
        filterServerList(tab, treeview, False)

        # If this is a re-sort, try to restore selection.
        if reSort:
            # Repopulating the treeview can take a long time if we have lots of servers
            # If the user already selected something, don't do anything
            if selected and treeview.exists(selected) and not treeview.focus():
                treeview.selection_set(selected)
                treeview.focus(selected)
                treeview.see(selected)

        else:
            # If this is not a re-sort, scroll to top
            treeview.yview_moveto(0)

        print('Finished sorting.')

        threadLockServerList.release()

    # Gets the info of the specified server and adds it to the treeview
    def getServerInfo(tab, treeview, server: tuple, quick: bool = False):
        global main_thread_exiting
        global GAMEDIR
        nonlocal disqualifiedServers

        server_addr = server[0] + ':' + str(server[1])

        # Don't query a server if it's already been disqualified --
        # i.e. for another game or mod
        if server_addr in disqualifiedServers:
            print(f'Skipping server {server}: Previously disqualified.')
            return

        try:
            server_info = gs.a2s_info(server, timeout=1)
        except Exception as err:
            print("Error querying game server " + str(server) + ": " + str(err))
            return

        # Filter out servers whose gamedir doesn't match.
        # Particularly useful for favorites from other mods
        if "folder" not in server_info or server_info["folder"] != GAMEDIR:
            threadLockServerList.acquire()

            print(f"Skipping server {server}: folder unset or does not match {GAMEDIR}")
            disqualifiedServers.append(server_addr)
            
            threadLockServerList.release()
            return

        if "name" not in server_info:
            print(f"Skipping server {server}: No name")
            return

        if "map" not in server_info:
            print(f"Skipping server {server}: No map")
            return

        if main_thread_exiting:
            return

        threadLockServerList.acquire()

        # Add the server to the list and treeview, only if a full refresh. (Quick Refresh does not clear server list)
        if not quick:
            if tab == "all":
                if server_addr in serverList:
                    print(f'Server {server_addr} already in server list')
                    threadLockServerList.release()
                    return

                serverList[server_addr] = server_info

            if tab == "favs":
                if server_addr in favoritesList:
                    print(f'Server {server_addr} already in favorites list')
                    threadLockServerList.release()
                    return

                favoritesList[server_addr] = server_info

            # Add the server to the treeview, but only if it passes filter settings
            if serverInFilterScope(tab, server_info):
                treeview.insert( '', 'end', iid=server_addr, values=serverToColumn(server_info) )

        else:
            # Doing quick refresh, update server lists and treeview data
            if tab == "all":
                serverList[server_addr] = server_info

            if tab == "favs":
                favoritesList[server_addr] = server_info

            if serverInFilterScope(tab, server_info):
                if treeview.exists(server_addr):
                    treeview.item(server_addr, values=serverToColumn(server_info))
                else:
                    treeview.insert( '', 'end', iid=server_addr, values=serverToColumn(server_info) )

            else:
                # Doesn't match filter settings, delete from treeview
                if treeview.exists(server_addr):
                    treeview.delete(server_addr)


        # Update the count of servers in the treeview header
        updateServerCount(tab, treeview)

        threadLockServerList.release()

    def refreshServerList(tab, treeview, quick, btnQuickRefresh, btnRefresh):
        global main_thread_exiting
        nonlocal threadLockServerList
        nonlocal threadLockFiltering
        nonlocal server_filter_pending
        nonlocal serverList
        nonlocal favoritesList
        nonlocal refresh_in_progress
        nonlocal stop_refresh

        if quick:
            btnQuickRefresh.config(text="Stop")
            btnRefresh.config(state="disabled")

        else:
            btnQuickRefresh.config(state="disabled")
            btnRefresh.config(text="Stop")

            # If there is an in-progress filter, these can take a while
            # Signal to cancel it
            if threadLockFiltering.locked():
                server_filter_pending = True

            # Wait for a lock before clearing the list
            # Avoids a conflict with filter operations
            threadLockServerList.acquire()
            treeview.delete(*treeview.get_children())
            server_filter_pending = False
            threadLockServerList.release()

        query_threads = []

        if tab == "all":
            if quick:
                server_addrs = []

                for server in serverList:
                    server = server.split(':')
                    server = ( str(server[0]), int(server[1]) )
                    server_addrs.append(server)

            else:
                try:
                    server_addrs = get_servers()
                except Exception as err:
                    print('Failed to obtain server list from Valve master servers: ' + str(err))
                    btnQuickRefresh.config(state="normal")
                    btnRefresh.config(state="normal")
                    return

                serverList = {}

        if tab == "favs":
            server_addrs = get_favorites()
            favoritesList = {}

        updateServerCount(tab, treeview)

        try:
            for server in server_addrs:
                while True:

                    refresh_in_progress = True

                    # Remove inactive threads from the list
                    for thread in query_threads:
                        if not thread.is_alive():
                            query_threads.remove(thread)

                    # Limit to 8 threads
                    if len(query_threads) >= 8:
                        time.sleep(0.1)
                        continue

                    if stop_refresh:
                        break

                    if main_thread_exiting:
                        return

                    print('Querying server ', str(server))

                    # Create new thread to query the server
                    thread = Thread(target=getServerInfo, args=(tab, treeview, server, quick))
                    thread.start()
                    query_threads.append(thread)
                    
                    # limit how quickly servers can be pinged
                    # avoid overwhelming the user's internet connection
                    # 0.02 = max 3,000 pings/minute
                    time.sleep(0.02)

                    break

        except Exception as err:
            print('Unhandled error querying servers: ' + str(err))
            btnQuickRefresh.config(state="normal")
            btnRefresh.config(state="normal")
            btnQuickRefresh.config(text="Quick Refresh")
            btnRefresh.config(text="Refresh All")
            return

        # wait for the outstanding threads to finish
        for thread in query_threads:

            if main_thread_exiting:
                print('Waiting on thread ' + str(thread))
            
            thread.join()

        if preferences['sortafterrefresh'].get():
            sortServerList(tab, treeview)

        # Return if the main window has closed
        if main_thread_exiting:
            return

        btnQuickRefresh.config(state="normal")
        btnRefresh.config(state="normal")
        btnQuickRefresh.config(text="Quick Refresh")
        btnRefresh.config(text="Refresh All")

        refresh_in_progress = False

        print('Server list refresh complete.')

    def refreshSelectedServer(treeview):
        # Get server addr from the iid of the selected row
        server_str = treeview.selection()[0]

        # convert to pair
        server = server_str.split(':')
        server = ( str(server[0]), int(server[1]) )

        # Query the server for game info
        try:
            server_info = gs.a2s_info(server)
        except Exception as err:
            print("Error querying game server " + str(server) + ": " + str(err))
            return

        treeview.item(server_str, values=serverToColumn(server_info))

    def refreshCallback(tab, treeview, quick, btnQuickRefresh, btnRefresh):
        nonlocal threadLockFiltering
        nonlocal refresh_in_progress
        nonlocal stop_refresh

        if refresh_in_progress:
            print('Signalling to stop refresh')
            btnQuickRefresh.config(state='disabled')
            btnRefresh.config(state='disabled')
            stop_refresh = True
        else:
            stop_refresh = False

            if threadLockFiltering.locked():
                print('Sorting in progress, asking to quit')
                server_filter_pending = True

            Thread( target=refreshServerList, args=(tab, treeview, quick, btnQuickRefresh, btnRefresh) ).start()

    def refreshSelectedServerCallback(treeview):
        nonlocal refresh_in_progress

        if not refresh_in_progress:
            Thread( target=refreshSelectedServer, args=(treeview,) ).start()

    def sortCallback(tab, treeview, column):
        nonlocal threadLockFiltering
        nonlocal server_filter_pending

        # If the lock is already acquired, signal to the existing operation to exit
        if threadLockFiltering.locked():
            print('Sorting in progress, asking existing sort to quit')
            server_filter_pending = True

        Thread( target=sortServerList, args=(tab, treeview, column) ).start()

    def filterCallback(tab, treeview):
        nonlocal threadLockFiltering
        nonlocal server_filter_pending

        # If the lock is already acquired, signal to the existing operation to exit
        if threadLockFiltering.locked():
            print('Sorting in progress, asking existing sort to quit')
            server_filter_pending = True

        Thread( target=filterServerList, args=(tab, treeview) ).start()

    def createTab(parent, tab: str):

        isTabAll = True if tab == "all" else False
        isTabFavs = True if tab == "favs" else False

        # Frame that holds the treeview and scrollbar
        treeview_frame = tk.Frame(parent)
        treeview_frame.grid(column=0, row=0, sticky="nsew")

        # Custom style without border for the treeview
        treeview_style = ttk.Style()
        treeview_style.layout(
            'Edge.Treeview',
            [('Edge.Treeview.treearea', {'sticky': 'nsew'})],
        )
        treeview_style.configure('Edge.Treeview', highlightthickness=0, bd=0)

        # Treeview for the server listing
        treeview = ttk.Treeview(treeview_frame, columns=("password", "vac", "name", "game", "map", "players", "latency"), selectmode="browse", style="Edge.Treeview")
        treeview.bind('<Button-1>', no_column_resize)

        treeview.column("#0", minwidth=0, width=0, stretch=False)
        treeview.heading("password", text="🔒", command = lambda: sortCallback(tab, treeview, "visibility"))
        treeview.column("password", minwidth=32, width=32, anchor="center", stretch=False)
        treeview.heading("vac", text="VAC", command = lambda: sortCallback(tab, treeview, "vac"))
        treeview.column("vac", minwidth=48, width=48, anchor="center", stretch=False)
        treeview.heading("name", text=" Server", anchor="w", command = lambda: sortCallback(tab, treeview, "name"))
        treeview.column("name", minwidth=128, anchor="w")
        treeview.heading("game", text=" Game", anchor="w", command = lambda: sortCallback(tab, treeview, "game"))
        treeview.column("game", minwidth=128, width=160, anchor="w", stretch=False)
        treeview.heading("map", text=" Map", anchor="w", command = lambda: sortCallback(tab, treeview, "map"))
        treeview.column("map", minwidth=128, width=160, anchor="w", stretch=False)
        treeview.heading("players", text=" Players", anchor="w", command = lambda: sortCallback(tab, treeview, "players"))
        treeview.column("players", minwidth=72, width=72, anchor="w", stretch=False)
        treeview.heading("latency", text=" Ping", anchor="w", command = lambda: sortCallback(tab, treeview, "_ping"))
        treeview.column("latency", minwidth=48, width=48, anchor="w", stretch=False)

        treeview.grid(column=0, row=0, sticky="nsew")

        # Scrollbar
        treeview_scroll = ttk.Scrollbar(treeview_frame,
           orient = "vertical", 
           command = treeview.yview)
        treeview_scroll.grid(column=1, row=0, sticky="ns")

        treeview.configure(yscrollcommand=treeview_scroll.set)

        treeview_frame.grid_rowconfigure(0, weight=1)
        treeview_frame.grid_columnconfigure(0, weight=1)

        # Frame for the buttons
        button_frame = ttk.Frame(parent)
        button_frame.grid(column=0, row=1, sticky="ew")

        # Adding items to the frame
        if isTabAll:
            btnFilters = ttk.Button(button_frame, text="Filters")
            btnFilters.grid(row=0, column=0, sticky="w", padx=4, pady=4)
            btnFilters.bind("<Enter>", showTooltip)
            btnFilters.bind("<Leave>", hideTooltip)
        else: 
            lblFavorites = ttk.Label(button_frame, text="Favorites from GoldenEye: Source will appear above")
            lblFavorites.grid(row=0, column=0, sticky="w", padx=4, pady=4)

        btnQuickRefresh = ttk.Button(button_frame, text="Quick Refresh", command = lambda: refreshCallback(tab, treeview, True, btnQuickRefresh, btnRefresh) )
        btnRefresh = ttk.Button(button_frame, text="Refresh All", command = lambda: refreshCallback(tab, treeview, False, btnQuickRefresh, btnRefresh) )
        btnConnect = ttk.Button(button_frame, text="Connect", command = lambda: connectToServer(treeview.selection()[0]))

        btnQuickRefresh.grid(row=0, column=1, sticky="e", padx=4, pady=4)
        btnRefresh.grid(row=0, column=2, sticky="e", padx=4, pady=4)
        btnConnect.grid(row=0, column=3, sticky="e", padx=4, pady=4)

        # Tooltips for the buttons
        btnQuickRefresh.bind("<Enter>", showTooltip)
        btnQuickRefresh.bind("<Leave>", hideTooltip)
        btnRefresh.bind("<Enter>", showTooltip)
        btnRefresh.bind("<Leave>", hideTooltip)
        btnConnect.bind("<Enter>", showTooltip)
        btnConnect.bind("<Leave>", hideTooltip)

        button_frame.grid_columnconfigure(0, weight=1)

        # Right-click server menu
        mnuServerOpts = Menu(parent, tearoff=0)
        mnuServerOpts.add_command(label="Connect to server", command = lambda: connectToServer(treeview.selection()[0]) )
        mnuServerOpts.add_command(label="View server info", command = lambda: windowServerInfo(root, treeview) )
        mnuServerOpts.add_command(label="Refresh server", command = lambda: refreshSelectedServerCallback(treeview) )

        # This is the menu for the filter options
        if isTabAll:
            mnuFilters = Menu(parent, tearoff=0)
            mnuFilters.add_checkbutton(label="Server not full", onvalue=1, offvalue=0, variable=preferences['notfull'], command=lambda: filterCallback(tab, treeview) )
            mnuFilters.add_checkbutton(label="Has players", onvalue=1, offvalue=0, variable=preferences['notempty'], command=lambda: filterCallback(tab, treeview) )
            mnuFilters.add_checkbutton(label="Is not password protected", onvalue=1, offvalue=0, variable=preferences['nopassword'], command=lambda: filterCallback(tab, treeview) )
            mnuFilters.add_checkbutton(label="VAC secured", onvalue=1, offvalue=0, variable=preferences['secure'], command=lambda: filterCallback(tab, treeview) )

            btnFilters.configure(command = lambda: showFilterMenu(btnFilters, mnuFilters))

        # Make the treeview do stuff
        treeview.bind('<Double-Button-1>', treeviewDoubleClick)
        treeview.bind("<Button-3>", lambda event: showServerOptsMenu(event, mnuServerOpts))
        treeview.bind("<Menu>", lambda event: showServerOptsMenu(event, mnuServerOpts))
        treeview.bind('<FocusIn>', focus_treeview)

        # Options menu
        mnuOpts = Menu(root, tearoff=0)
        mnuOpts.add_checkbutton(label="Close window on connection to server", onvalue=1, offvalue=0, variable=preferences['closeonconnect'])
        mnuOpts.add_checkbutton(label="Re-sort server list after refresh", onvalue=1, offvalue=0, variable=preferences['sortafterrefresh'])

        button_frame.bind("<Button-3>", lambda event: showOptsMenu(event, mnuOpts))

        if isTabFavs:
            lblFavorites.bind("<Button-3>", lambda event: showOptsMenu(event, mnuOpts))

        # Configure row and column weights
        parent.grid_rowconfigure(0, weight=1)
        parent.grid_rowconfigure(1, weight=0)
        parent.grid_columnconfigure(0, weight=1)

        # Initial refresh
        parent.after(0, lambda: refreshCallback(tab, treeview, False, btnQuickRefresh, btnRefresh))

    root = tk.Tk()
    root.minsize( scaled(700), scaled(300) )
    root.title("GoldenEye: Source Servers")
    root.protocol("WM_DELETE_WINDOW", close_program)

    if platform.system() == "Windows":
        root.iconbitmap( resource_path("icon.ico") )

    init_prefs()

    # Tab controls
    tabControl = ttk.Notebook(root)
    tabAllServers = ttk.Frame(tabControl)
    tabFavorites = ttk.Frame(tabControl)

    tabControl.add(tabAllServers, text="All Servers")
    tabControl.add(tabFavorites, text="Favorites")

    tabControl.grid(column=0, row=0, sticky="nsew", padx=10, pady=10)

    # Create the controls that will be inside the tabs
    createTab(tabAllServers, "all")
    createTab(tabFavorites, "favs")

    # Configure grid
    tabControl.grid_rowconfigure(0, weight=1)
    tabControl.grid_columnconfigure(0, weight=1)

    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)

    # Show window
    root.mainloop()

main()