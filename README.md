# GoldenEye: Source Server Browser

This is a server browser for GE:S. It allows you to view game servers without launching the game. The interface closely resembles the in-game server browser.

## Features:

* See the server list (equivalent to the "Internet" tab in-game)
* Favorites tab
* Basic pre-defined filters (has players, not full, etc)
* View server info (game info, connected players, etc)
* Connect to servers

![Screenshot](screenshot.png)

## Known Issues

* The code is pretty bad :) but it's functional