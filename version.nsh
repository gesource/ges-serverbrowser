!define PROD_NAME "GoldenEye: Source Server Browser"
; Product name without any special chars, used for file names
!define FILE_NAME "GoldenEye Source Server Browser"

; Product version, MUST have 4 parts
!define PROD_VERSION "1.4.0.0"

!define INSTALL_DIR "$PROGRAMFILES64\${FILE_NAME}"