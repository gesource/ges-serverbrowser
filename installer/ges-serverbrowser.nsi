!include "${__FILEDIR__}\..\version.nsh"
!include nsDialogs.nsh

!define UNINSTALL_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${FILE_NAME}"

Name "${PROD_NAME}"
OutFile "${__FILEDIR__}\..\dist\ges-serverbrowser-v${PROD_VERSION}-setup.exe"

VIProductVersion "${PROD_VERSION}"
VIAddVersionKey "ProductName" "${PROD_NAME}"
VIAddVersionKey "FileDescription" "${PROD_NAME} Setup"
VIAddVersionKey "FileVersion" "1.0.0.0"
VIAddVersionKey "ProductVersion" "${PROD_VERSION}"
VIAddVersionKey "LegalCopyright" ""

SetCompressor /SOLID lzma

RequestExecutionLevel admin
ManifestDPIAware true

InstallDir "${INSTALL_DIR}"

Page custom page_welcome
Page instfiles

Function page_welcome

    nsDialogs::Create 1018
        Pop $0

        ${NSD_CreateLabel} 0 0 100% 100% \
            "You're about to install ${PROD_NAME}."

    nsDialogs::Show

FunctionEnd

Section install

    SetShellVarContext all

    nsExec::Exec 'powershell.exe -c "exit (Get-CimInstance -ClassName Win32_Process -Filter \"Name = $\'ges-serverbrowser.exe$\'\").ProcessID"'
    Pop $0

    DetailPrint "PowerShell returned $0"

    StrCmp $0 0 NotRunning
        SetDetailsView show
        MessageBox MB_OK "${PROD_NAME} is currently running. Please close it and try again."
        Quit

    NotRunning:

    ; Remove old version directory
    IfFileExists "$INSTDIR\*.*" +1 CreateInstdir
        RMDir /r $INSTDIR

        IfErrors +1 NoErrors
            MessageBox MB_OK "An error occurred removing existing folder $\r$\n$INSTDIR$\r$\n$\r$\n\
                Make sure the files are not in use, then try running this installer again."
            Abort

        NoErrors:

    ; start install
    CreateInstdir:
    CreateDirectory $INSTDIR

    SetOutPath $INSTDIR

    File /r ..\dist\ges-serverbrowser\*

    CreateShortCut "$SMPROGRAMS\${FILE_NAME}.lnk" \
        "$INSTDIR\ges-serverbrowser.exe"

    WriteINIStr "$SMPROGRAMS\desktop.ini" "LocalizedFileNames" "${FILE_NAME}.lnk" "${PROD_NAME}"

    IfErrors +1 NoWriteErrors
        SetDetailsView show

        MessageBox MB_OK|MB_ICONEXCLAMATION \
            "Error writing files. Check log."

        Abort

    NoWriteErrors:

SectionEnd

Section writeUninstaller

    WriteUninstaller $INSTDIR\uninstall.exe

    WriteRegStr HKLM "${UNINSTALL_KEY}" \
        "DisplayName" "${PROD_NAME}"

    WriteRegStr HKLM "${UNINSTALL_KEY}" \
        "UninstallString" "$INSTDIR\uninstall.exe"

    WriteRegStr HKLM "${UNINSTALL_KEY}" \
        "DisplayIcon" "$INSTDIR\ges-serverbrowser.exe"

SectionEnd

; Uninstall sections
SilentUnInstall silent

UninstPage instfiles

Section un.install

    SetShellVarContext all

    nsExec::Exec 'powershell.exe -c "exit (Get-CimInstance -ClassName Win32_Process -Filter \"Name = $\'ges-serverbrowser.exe$\'\").ProcessID"'
    Pop $0

    StrCmp $0 0 NotRunning
        MessageBox MB_OK "${PROD_NAME} is currently running. Please close it and try again."
        Quit

    NotRunning:

    RMDir /r "${INSTALL_DIR}"
    Delete "$SMPROGRAMS\${FILE_NAME}.lnk"
    DeleteINIStr "$SMPROGRAMS\desktop.ini" "LocalizedFileNames" "${FILE_NAME}.lnk"
    DeleteRegKey HKLM "${UNINSTALL_KEY}"

SectionEnd