@SET "PATH=C:\Program Files (x86)\NSIS\Bin;%PATH%

@echo Remember to update version.txt AND version.nsh!
@pause

@echo Building EXE in onefile mode
pyinstaller.exe --name=ges-serverbrowser-portable --version-file=version.txt --clean --onefile --windowed --icon=icon.ico --add-data="icon.ico:." serverbrowser.py

@echo Building EXE in onedir mode
pyinstaller.exe --name=ges-serverbrowser --version-file=version.txt --onedir --noconfirm --windowed --icon=icon.ico --add-data="icon.ico:." serverbrowser.py

@echo Building installer
makensis /NOCD installer\ges-serverbrowser.nsi

@echo Deleting temp files
del ges-serverbrowser.spec
del ges-serverbrowser-portable.spec
rmdir /S /Q dist\ges-serverbrowser

@echo.
@echo Output is in the 'dist' folder.
@pause
